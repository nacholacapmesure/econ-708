# ECON 708 - Economía y Procesamiento de Datos No Tradicionales

Segundo cuatrimestre 2022.

```
17  de Agosto, 2022.
```
  
Econ708 es un curso de introducción a la programación y al procesamiento de datos tradicionales y no tradicionales orientado a estudiantes de Economía en base al lenguaje [`R`](https://cran.r-project.org/). El curso tiene dos módulos. En la primera parte se desarrollan elementos de programación y manipulación de datos. La segunda parte cubre aplicaciones orientadas a la captura automática y procesamiento de datos, y análisis de redes.

> Docentes: Sergio De Raco y Viktoriya Semeshenko

> Versión: Segundo cuatrimestre 2022


## Objetivos de aprendizaje

* Entender los elementos y procedimientos básicos de la programación del lenguaje R.

* Comprender el flujo de trabajo de procesamiento de datos: cargar, emprolijar, manipular, analizar y visualizar datos.

* Conocer diferentes formas de la representación gráfica de información y datos, y comunicación de resultados.

* Familiarizarse con diferentes librerías del lenguaje de programación utilizado.

* Estimular la aplicación del abordaje computacional a problemas de interés económico y social de diversa índole.


## Requerimientos

- **Formales**: Matemática para Economistas (Código 288) y Estadística II (Código 285).

- **Deseables**: Interés por el trabajo participativo en equipo y el aprendizaje mediante prueba y error.

- **Técnicos**: Notebook personal con min 8Gb RAM.


## Criterio de evaluación

Para la aprobación del curso se requiere de la elaboración y aprobación de un proyecto de datos final.


## Resumen de contenidos

* *Unidad 0. Instalación y setup de workplace para proyectos de datos en R*. Lenguaje R y entorno en Docker. Interfase de línea de comandos (CLI) y RStudio.

* *Unidad 1a. Elementos de programación en R*. Introducción a elementos de programación (dato, conjunto de datos). Gestión de variables y almacenamiento. Estructuras de datos (tipos). Operaciones (pivoting/merges para dataframes). Sentencias condicionales y estructuras de control de flujos: `for`, `while`, `if`. Implementación de funciones ad-hoc. Análisis estadístico descriptivo básico. R con tidyverse. Piping. Gestión de flujos de proyecto de datos (PD): Importar. Preprocesar (Transformar y Explorar). Visualizar. Modelizar. Comunicar.

* *Unidad 1b. Visualización y comunicación en R*. Nociones de graficación (forma, color, tamaño, color). Exploración y Visualización de datos. Introducción a ggplot, template. Casos: tidy datasets, Gapminder. Comunicación: Rmarkdown, Graphics, Notebooks, Presentaciones, Interactivos, Dashboards. 

* *Unidad 2a. Aplicaciones 1: Captura de datos no tradicionales*. Core Econ (Project 9/Project 10). Scraping 101 (rvest). CRAN vignette/ LADAL web crawling and scraping/Recursos para scraping en W3schools (tutoriales y selectores) 

* *Unidad 2b. Aplicaciones 2: Redes de interacción*. Introducción al análisis de redes en R. Statistical Analysis of Network Data. Tutorial Kateto. Aplicaciones: Redes Gutenberg/Redes socials ONA Book (API tweets British network) 

* *Unidad 2c. Aplicaciones 3 (EXTRA): Text mining*. Introducción al análisis de textos en R. Tidytext con novelas y datos de Twitter. 



## Fuentes consultadas

El material para el curso fue extraído y transformado de diversas fuentes:

-   [https://meghan.rbind.io/talk/neair/](https://meghan.rbind.io/talk/neair/)
-   [https://www.core-econ.org/doing-economics/](https://www.core-econ.org/doing-economics/)
-   [https://ladal.edu.au/webcrawling.html](https://ladal.edu.au/webcrawling.html)
-   [https://github.com/kolaczyk/sand](https://github.com/kolaczyk/sand)
-   [https://kateto.net/network-visualization](https://kateto.net/network-visualization)    
-   [https://paldhous.github.io/NICAR/2019/r-text-analysis.html](https://paldhous.github.io/NICAR/2019/r-text-analysis.html)
-   [https://ladal.edu.au/net.html](https://ladal.edu.au/net.html)
-   [https://github.com/keithmcnulty/ona_book](https://github.com/keithmcnulty/ona_book)
-   [https://psyteachr.github.io/hack-your-data/scrape-twitter.html](https://psyteachr.github.io/hack-your-data/scrape-twitter.html)
-   [https://juliasilge.github.io/tidytext/articles/topic_modeling.html](https://juliasilge.github.io/tidytext/articles/topic_modeling.html)


## Bibliografía básica 

+ [Wickham, H., & Grolemund, G. (2016). R for data science: import, tidy, transform, visualize, and model data. " O'Reilly Media, Inc.".](https://r4ds.had.co.nz/) en [español](https://es.r4ds.hadley.nz/index.html#sobre-la-versi%C3%B3n-original-en-ingl%C3%A9s).
+ [R Core Development Team, (2000), _Introducción a R. Notas sobre R: Un entorno de programación para Análisis de Datos y Gráficos_.]( https://cran.r-project.org/doc/contrib/R-intro-1.1.0-espanol.1.pdf).


# Entorno de trabajo

> Pasos para la instalación y configuración del software necesario (normalizado) para el curso. 

> IMPORTANTE: deben seguirse estos pasos independientemente de que tengan instalado el software (`R`, `RStudio`, `JupyterLab`) en cualquiera de sus versiones.

Dado que el curso se construye sobre el aprendizaje y uso de un lenguaje de programación, está pensado para que **cada alumno trabaje individualmente en una máquina**, que *asumiremos* propia en función de que sólo es posible adquirir estas habilidades a través de la práctica por medio de (¡muchas!) pruebas y errores.

Para desarrollar los contenidos del curso utilizaremos el software estadístico [`R`](https://cran.r-project.org/) y [`RStudio Desktop`](https://www.rstudio.com/products/rstudio/) como el entorno de desarrollo integrado (IDE) para la gestión de proyectos de datos, y los blocs de notas [`Jupyter`](https://jupyter.org/) como una herramienta multilenguaje alternativa en su propio IDE `JupyterLab`. Dado que las características de la instalación de estos software son dependientes del sistema operativo del equipo `host` en que se instalen (e.g.: Windows, Mac, Linux) usaremos servicios de contenedores `Docker` preparados con lo necesario, que correrán en el navegador de preferencia (e.g.: Chrome, Firefox, Safari). De esta manera, **sólo requeriremos instalar** el IDE [`Docker Desktop`](https://www.docker.com/products/docker-desktop/) y luego registrar un usuario en [`Docker Hub` ](https://hub.docker.com/) para descargar las imágenes de los contenedores necesarios, como se explicará más abajo.

Adicionalmente, requeriremos instalar [`git`](https://git-scm.com/downloads), una herramienta para el control de versiones que servirá para gestionar de manera eficiente el desarrollo de los proyectos de datos que encararemos en el curso.

> **Requerimientos mínimos de hardware**:

> - Procesador 64 bits

> - 8Gb de RAM

> - Capacidad de virtualización de hardware en BIOS

> - Mínimo de 15Gb de espacio disponible

A continuación detallamos los pasos a seguir para poner a punto el entorno de trabajo.

## Instalar Docker

1. Descargar e instalar la versión de [`Docker Desktop`]([https://docs.docker.com/desktop/](https://docs.docker.com/desktop/)) según el sistema operativo instalado en la máquina (elegir [acá](https://docs.docker.com/desktop/#download-and-install)). En Linux pedirá generar una clave `gpg`  de seguridad (seguir instrucciones cuando aparezca la ventana que solicita la clave).

2. Acceder a [`Docker Hub` ]([https://hub.docker.com/](https://hub.docker.com/)) y crear un usuario.

4. Ejecutar la aplicación Docker Desktop bajada en el paso 1.

5. Logearse con el usuario Docker Hub creado en el paso 2.

## Instalar git

1. Abrir una terminal y ejecutar el siguiente comando para chequear si ya está instalado:

``` bash
$ git
```

2. Si lo anterior presenta un error, en Windows descargar e instalar [`git`](https://git-scm.com/downloads), en Linux usar el administrador de paquetes correspondiente.

> Nota: [Acá](https://docs.microsoft.com/es-mx/devops/develop/git/install-and-set-up-git) se puede consultar una guía provista por Microsoft

## Clonar repo(sitorio) del curso

El repo del curso está en https://gitlab.com/sderaco/econ-708 y se irá actualizando a lo largo de la cursada. Para usarlo es necesario "clonarlo" con `git` en un directorio de trabajo que elijan ustedes.

1. Elegir un directorio de trabajo (p.ej.: `<Usuario>/Documentos/Facultad`) y abrir una `terminal` en esa posición.

2. Ejecutar

``` bash
$ git clone https://gitlab.com/sderaco/econ-708
```

> Nota: La carpeta `econ-708` contiene la información que compartiremos del curso y una carpeta `proyectos` para que guarden su trabajo y sea visible para las aplicaciones que instalaremos en contenedores.

El repositorio clonado tiene la siguiente estructura de directorios: 

![](https://lh5.googleusercontent.com/CcQO6_gbC9hdisB_RSN17MGOIT9egG2OgLoTE4rq_RqQJUTbzEpxGhatScCqTW7cgN8tqa5o8KA-74nKDhvjlL5rKCrvRykPXy0VLeZY4m_J1crFkKgYStvzuFXr7WHFYns3rTmZkyUiBL9nSTCs6Vs)

Para la elaboración del proyecto final deberán crear su proyecto en la carpeta `proyectos/proyecto_final` en base a las indicaciones que daremos oportunamente.


## Crear contenedores

Para descargar las aplicaciones que utlizaremos (`R`, `RStudio` y `Juyter`) llamaremos a los contenedores desde la `terminal` y luego abriremos un navegador para usarlos.

1. Abrir una `terminal` en la carpeta `econ-708` clonada.

2. Crear los contenedores preparados mediante el siguiente comando:

``` bash
$ docker compose up -d --build --remove-orphans
```

> Nota: Esta operación descargará los contenedores de `Docker Hub` y es **intensiva en uso de datos (varios Gb!)**, se sugiere **estar conectado a una red WiFi**.

Al final del proceso, en Docker Desktop aparecerán los contenedores de esta forma:

![](https://lh4.googleusercontent.com/xGPIFhf5sS09AS3Q9eWUenyruZV_0FEzTyM-8aCzs8GaqD6rvtyY2AuZOFvBvxvylULCS0_tKg_3krRtFZ65hm9A4ZVfV3Eql3fBXa1kBG4C0Cu7vCLgU2X89W-uyXgCuYQrpbrizXjaZeHc-E8Apr8)

  

## Ejecutar aplicaciones instaladas

1. Abrir el navegador de internet preferido.

2. A. Para usar `RStudio`: en el navegador abrir una pestaña nueva (`Ctrl/Cmd + T`) y dirigirse a la dirección http://localhost:8787/.

2. B. Para usar `JupyterLab`: en el navegador abrir una pestaña nueva (`Ctrl/Cmd + T`) y dirigirse a la dirección http://localhost:8888/. Escribir `econ-708` en la casilla que pide contraseña.


